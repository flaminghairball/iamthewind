using Zenject;
using NUnit.Framework;
using UnityEngine;
using IATW.Gameplay;
using System.Linq;

namespace Tests.IATW.Gameplay {

[TestFixture]
public class PathGeneratorTests : ZenjectUnitTestFixture
{
    [Test]
    public void PathGenerator_CreatesFirstPoint()
    {
        //arrange
        var settings = new PathGenerator.Settings();
        settings.minDistanceBetweenPoints = 1.0f;
        var pathGenerator = new PathGenerator(settings);

        //act
        pathGenerator.AddSample(Vector3.zero, 0.0f);

        //assert
        Assert.That(pathGenerator.path.Count == 1, "Path generator should create first point when supplied with first sample");
    }

    [Test]
    public void PathGenerator_CreatesSecondPoint_WhenMinDistanceExceeded()
    {
        //arrange
        var settings = new PathGenerator.Settings();
        settings.minDistanceBetweenPoints = 1.0f;
        var pathGenerator = new PathGenerator(settings);

        //act
        pathGenerator.AddSample(Vector3.zero, 0.0f);
        pathGenerator.AddSample(Vector3.right * 1.1f, 0.0f);

        //assert
        Assert.That(pathGenerator.path.Count == 2, "Path generator should create a second point with a sample over minDistanceBetweenPoints");
    }

    [Test]
    public void PathGenerator_CreatesSecondPoint_WhenMinDistanceExceededAfterInBetweenSample()
    {
        //arrange
        var settings = new PathGenerator.Settings();
        settings.minDistanceBetweenPoints = 1.0f;
        var pathGenerator = new PathGenerator(settings);

        //act
        pathGenerator.AddSample(Vector3.zero, 0.0f);
        pathGenerator.AddSample(Vector3.right * 0.5f, 0.0f);
        pathGenerator.AddSample(Vector3.right * 1.1f, 0.0f);
        pathGenerator.AddSample(Vector3.right * 1.2f, 0.0f);

        //assert
        Assert.That(pathGenerator.path.Count == 3, "Path generator should create a second point when a third sample is added that exceeds minDistanceBetweenPoints after a second sample that did not exceed minDistanceBetweenPoints");
    }

    [Test]
    public void PathGenerator_TracksLastSample()
    {
        //arrange
        var settings = new PathGenerator.Settings();
        settings.minDistanceBetweenPoints = 1.0f;
        var pathGenerator = new PathGenerator(settings);

        //act
        pathGenerator.AddSample(Vector3.zero, 0.0f);
        pathGenerator.AddSample(Vector3.right * 0.5f, 0.0f);
        

        //assert
        Assert.That(Vector3.Distance(pathGenerator.path.Last(), Vector3.right * 0.5f) < 0.05f, "Last point in path should be last sample supplied to path generator");
    }

    [Test]
    public void PathGenerator_RespectsMinDistanceBetweenPoints()
    {
        //arrange
        var settings = new PathGenerator.Settings();
        settings.minDistanceBetweenPoints = 1.0f;
        var pathGenerator = new PathGenerator(settings);

        //act
        pathGenerator.AddSample(Vector3.zero, 0.0f);
        pathGenerator.AddSample(Vector3.right * 0.5f, 0.0f);
        pathGenerator.AddSample(Vector3.up * 0.75f, 0.0f);
        
        //assert
        Assert.That(pathGenerator.path.Count == 2, "Path generator should not add points for samples until minDistance is exceeded");
    }

    [Test]
    public void PathGenerator_ClearsPath_WhenCleared()
    {
        //arrange
        var settings = new PathGenerator.Settings();
        settings.minDistanceBetweenPoints = 1.0f;
        var pathGenerator = new PathGenerator(settings);

        //act
        pathGenerator.AddSample(Vector3.zero, 0.0f);
        pathGenerator.AddSample(Vector3.right * 0.5f, 0.0f);
        pathGenerator.AddSample(Vector3.up * 0.75f, 0.0f);
        pathGenerator.Clear();

        //assert
        Assert.That(pathGenerator.path.Count == 0, "Path generator should have no points after clearing");
    }

    [Test]
    public void PathGenerator_FiresUpdate_AfterAddingFirstSample()
    {
        //arrange
        var settings = new PathGenerator.Settings();
        settings.minDistanceBetweenPoints = 1.0f;
        var pathGenerator = new PathGenerator(settings);

        var wasEventFiredAfterAddingPoint = false;

        pathGenerator.onPathUpdated += (path => {
            wasEventFiredAfterAddingPoint = path.Last() == Vector3.one;
        });

        //act
        pathGenerator.AddSample(Vector3.one, 0.0f);

        //assert
        Assert.That(wasEventFiredAfterAddingPoint, "Path generator should fire OnPathUpdate after adding sample");
    }


    [Test]
    public void PathGenerator_FiresUpdate_AfterAddingSecondSample()
    {
        //arrange
        var settings = new PathGenerator.Settings();
        settings.minDistanceBetweenPoints = 1.0f;
        var pathGenerator = new PathGenerator(settings);

        var wasEventFiredAfterAddingPoint = false;

        //act
        pathGenerator.AddSample(Vector3.one, 0.0f);
        pathGenerator.onPathUpdated += (path => {
            wasEventFiredAfterAddingPoint = path.Last() == Vector3.one * 2f;
        });
        pathGenerator.AddSample(Vector3.one * 2f, 0.0f);


        //assert
        Assert.That(wasEventFiredAfterAddingPoint, "Path generator should fire OnPathUpdate after adding second sample");
    }


    [Test]
    public void PathGenerator_FiresUpdate_AfterClearing()
    {
        //arrange
        var settings = new PathGenerator.Settings();
        settings.minDistanceBetweenPoints = 1.0f;
        var pathGenerator = new PathGenerator(settings);
        var wasEventFiredAfterClearingPath = false;

        //act
        pathGenerator.AddSample(Vector3.one, 0.0f);

        pathGenerator.onPathUpdated += (path => {
            wasEventFiredAfterClearingPath = path.Count == 0;
        });
        pathGenerator.Clear();

        //assert
        Assert.That(wasEventFiredAfterClearingPath, "Path generator should fire OnPathUpdate after clearing");
    }
}

}