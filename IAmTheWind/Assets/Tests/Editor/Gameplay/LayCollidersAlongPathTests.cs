using Zenject;
using NUnit.Framework;
using UnityEngine;
using IATW.Gameplay;
using System.Linq;
using System.Collections.Generic;

namespace Tests.IATW.Gameplay {

[TestFixture]
public class LayCollidersAlongPathTests : ZenjectUnitTestFixture
{
    [Test]
    public void SetPath_ObservesColliderCount()
    {
        //arrange
        var colliderPrefab = new GameObject("Collider").AddComponent<CapsuleCollider>();
        var settings = new LayCollidersAlongPath.Settings();
        settings.colliderPrefab = colliderPrefab;
        settings.colliderCount = 4;
        var colliderPath = new LayCollidersAlongPath(settings);
        
        //assert
        Assert.AreEqual(4, colliderPath.colliders.Length, "Initializing collider path with a collider count of 4 should result in a 4-length array of generated colliders");
    }

    [Test]
    public void SetPath_WithNoPoints_DisablesAllColliders()
    {
        //arrange
        var colliderPrefab = new GameObject("Collider").AddComponent<CapsuleCollider>();
        var settings = new LayCollidersAlongPath.Settings();
        settings.colliderPrefab = colliderPrefab;
        settings.colliderCount = 4;
        var colliderPath = new LayCollidersAlongPath(settings);

        var path = new List<Vector3>();

        //act
        colliderPath.SetPath(path);

        //assert
        Assert.That(colliderPath.colliders.All(x => !x.enabled), "A zero-point path should have no enabled colliders");
    }

    [Test]
    public void SetPath_WithOnePoint_DisablesAllColliders()
    {
        //arrange
        var colliderPrefab = new GameObject("Collider").AddComponent<CapsuleCollider>();
        var settings = new LayCollidersAlongPath.Settings();
        settings.colliderPrefab = colliderPrefab;
        settings.colliderCount = 4;
        var colliderPath = new LayCollidersAlongPath(settings);
        
        var path = new List<Vector3>();
        path.Add(Vector3.zero);

        //act
        colliderPath.SetPath(path);

        //assert
        Assert.That(colliderPath.colliders.All(x => !x.enabled), "A one-point path should have no enabled colliders");
    }

    [Test]
    public void SetPath_WithTwoPoints_HasOneEnabledCollider()
    {
        //arrange
        var colliderPrefab = new GameObject("Collider").AddComponent<CapsuleCollider>();
        var settings = new LayCollidersAlongPath.Settings();
        settings.colliderPrefab = colliderPrefab;
        settings.colliderCount = 4;
        var colliderPath = new LayCollidersAlongPath(settings);
        
        var path = new List<Vector3>();
        path.Add(Vector3.zero);
        path.Add(Vector3.one);

        //act
        colliderPath.SetPath(path);

        //assert
        Assert.AreEqual(1, colliderPath.colliders.Count(x => x.enabled), "A one-point path should have no enabled colliders");
    }

    [Test]
    public void SetPath_WithTwoPoints_PlacesColliderAtMidpoint()
    {
        //arrange
        var colliderPrefab = new GameObject("Collider").AddComponent<CapsuleCollider>();
        var settings = new LayCollidersAlongPath.Settings();
        settings.colliderPrefab = colliderPrefab;
        settings.colliderCount = 4;
        var colliderPath = new LayCollidersAlongPath(settings);
        
        var path = new List<Vector3>();
        path.Add(Vector3.zero);
        path.Add(Vector3.right);

        //act
        colliderPath.SetPath(path);

        //assert
        Assert.AreEqual(Vector3.right * 0.5f, colliderPath.colliders[0].transform.position, "A one-point path should have no enabled colliders");
    }

    [Test]
    public void SetPath_WithTwoPoints_RotatesColliderProperly()
    {
        //collider bottom should be at point 1, top at point 2

        //arrange
        var colliderPrefab = new GameObject("Collider").AddComponent<CapsuleCollider>();
        var settings = new LayCollidersAlongPath.Settings();
        settings.colliderPrefab = colliderPrefab;
        settings.colliderCount = 4;
        var colliderPath = new LayCollidersAlongPath(settings);
        
        var path = new List<Vector3>();
        path.Add(Vector3.zero);
        path.Add(Vector3.right + Vector3.up);

        //act
        colliderPath.SetPath(path);

        //assert
        var angle = Vector3.Angle(colliderPath.colliders[0].transform.up, Vector3.up + Vector3.right);
        Assert.AreEqual(0, angle, "Collider's up vector3 should align with vector pointing from point a to point b of path");
    }


    [Test]
    public void SetPath_WithTwoPoints_SizesColliderProperly()
    {
        //arrange
        var colliderPrefab = new GameObject("Collider").AddComponent<CapsuleCollider>();
        var settings = new LayCollidersAlongPath.Settings();
        settings.colliderPrefab = colliderPrefab;
        settings.colliderCount = 4;
        var colliderPath = new LayCollidersAlongPath(settings);
        
        var path = new List<Vector3>();
        path.Add(Vector3.zero);
        path.Add(Vector3.right * 3f);    //collider height should be 3f

        //act
        colliderPath.SetPath(path);

        //assert
        Assert.AreEqual(3f, colliderPath.colliders[0].height, "A path from 0-3 should have a collider height of 3");
    }

    [Test]
    public void SetPath_WithTooManyPoints_DoesntThrow()
    {
        //arrange
        var colliderPrefab = new GameObject("Collider").AddComponent<CapsuleCollider>();
        var settings = new LayCollidersAlongPath.Settings();
        settings.colliderPrefab = colliderPrefab;
        settings.colliderCount = 4;
        var colliderPath = new LayCollidersAlongPath(settings);
        
        var path = new List<Vector3>();
        path.Add(Vector3.zero);
        path.Add(Vector3.one);
        path.Add(Vector3.one * 2);
        path.Add(Vector3.one * 3);
        path.Add(Vector3.one * 4);
        path.Add(Vector3.one * 5);  //6 points, 5 lines, but only 4 colliders!

        //act
        colliderPath.SetPath(path);

        //assert
        //just make sure an exception isn't thrown
    }
}

}