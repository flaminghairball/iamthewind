using UnityEngine;
using Zenject;
using IATW.GUI;

namespace IATW {

public class Level1SceneInstaller : MonoInstaller<Level1SceneInstaller>
{
    [SerializeField] Player player;
    [SerializeField] AnimatedScreen loseScreen;
    [SerializeField] AnimatedScreen winScreen;
    
    public override void InstallBindings()
    {
        Container.Bind<LevelSettings>().FromInstance(new LevelSettings { nextSceneName = "2"}).AsSingle();
        Container.Bind<LevelController>().AsSingle();
        Container.BindInterfacesAndSelfTo<Player>().FromInstance(player).AsSingle();

        Container.Bind<AnimatedScreen>().WithId(Constants.LoseScreenId).FromInstance(loseScreen);
        Container.Bind<AnimatedScreen>().WithId(Constants.WinScreenId).FromInstance(winScreen);

        Container.Resolve<LevelController>();
    }
}

}