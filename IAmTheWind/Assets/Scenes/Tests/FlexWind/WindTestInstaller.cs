using UnityEngine;
using Zenject;
using IATW.Gameplay;

[CreateAssetMenu(fileName = "WindTestInstaller", menuName = "Installers/WindTestInstaller")]
public class WindTestInstaller : ScriptableObjectInstaller<WindTestInstaller>
{
    [SerializeField] IATW.Gameplay.PathGenerator.Settings pathGeneratorSettings;
    [SerializeField] LineRenderer windLineRendererPrefab;

    public override void InstallBindings()
    {
        Container.Bind<Camera>().FromInstance(Camera.main).AsSingle();
        Container.Bind<LineRenderer>().WithId(IATW.Constants.WindLinePrefabId).FromComponentInNewPrefab(windLineRendererPrefab).AsSingle();
        Container.BindInterfacesAndSelfTo<MouseSampler>().AsSingle();
        Container.BindInterfacesAndSelfTo<PathGenerator>().AsSingle().WithArguments(pathGeneratorSettings);
        Container.BindInterfacesAndSelfTo<Wind>().AsSingle();

        Container.Resolve<Wind>();
    }
}