using UnityEngine;
using Zenject;
using IATW.Gameplay;

[CreateAssetMenu(fileName = "LinearWindTestInstaller", menuName = "Installers/LinearWindTestInstaller")]
public class LinearWindTestInstaller : ScriptableObjectInstaller<LinearWindTestInstaller>
{
    [SerializeField] IATW.Gameplay.LinePathGenerator.Settings pathGeneratorSettings;
    [SerializeField] IATW.Gameplay.ApplyForceAlongPath.Settings applyForceAlongPathSettings;
    [SerializeField] LineRenderer windLineRendererPrefab;

    public override void InstallBindings()
    {
        Container.Bind<Camera>().FromInstance(Camera.main).AsSingle();
        Container.Bind<LineRenderer>().WithId(IATW.Constants.WindLinePrefabId).FromComponentInNewPrefab(windLineRendererPrefab).AsSingle();
        Container.BindInterfacesAndSelfTo<MouseSampler>().AsSingle();
        Container.BindInterfacesAndSelfTo<LinePathGenerator>().AsSingle().WithArguments(pathGeneratorSettings);
        Container.BindInterfacesAndSelfTo<ApplyForceAlongPath>().AsSingle().WithArguments(applyForceAlongPathSettings);
        Container.BindInterfacesAndSelfTo<Wind>().AsSingle();

        Container.Resolve<Wind>();
    }
}