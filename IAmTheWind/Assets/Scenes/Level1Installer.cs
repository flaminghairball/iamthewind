using UnityEngine;
using Zenject;
using IATW;
using IATW.Gameplay;

[CreateAssetMenu(fileName = "Level1Installer", menuName = "Installers/Level1Installer")]
public class Level1Installer : ScriptableObjectInstaller<Level1Installer>
{
    [SerializeField] IATW.Gameplay.LinePathGenerator.Settings pathGeneratorSettings;
    [SerializeField] IATW.Gameplay.ApplyForceAlongPath.Settings applyForceAlongPathSettings;
    [SerializeField] LineRenderer windLineRendererPrefab;

    public override void InstallBindings()
    {
        Container.Bind<Camera>().FromInstance(Camera.main).AsSingle();
        Container.Bind<LineRenderer>().WithId(IATW.Constants.WindLinePrefabId).FromComponentInNewPrefab(windLineRendererPrefab).AsSingle();
        Container.BindInterfacesAndSelfTo<MouseSampler>().AsSingle();
        Container.BindInterfacesAndSelfTo<LinePathGenerator>().AsSingle().WithArguments(pathGeneratorSettings);
        Container.BindInterfacesAndSelfTo<ApplyForceAlongPath>().AsSingle().WithArguments(applyForceAlongPathSettings);
        Container.BindInterfacesAndSelfTo<Wind>().AsSingle();
        
        Container.Resolve<Wind>();
    }
}