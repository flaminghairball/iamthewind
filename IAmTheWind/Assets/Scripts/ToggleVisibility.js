var onObjects : GameObject[];
var offObjects : GameObject[];

function ToggleOn(){
	for(var curObj in offObjects){
		curObj.SetActiveRecursively(false);
	}
	for(var curObj in onObjects){
		curObj.SetActiveRecursively(true);
	}
}

function ToggleOff(){
	for(var curObj in offObjects){
		curObj.SetActiveRecursively(true);
	}
	for(var curObj in onObjects){
		curObj.SetActiveRecursively(false);
	}
}