var linePrefab : GameObject;

var spawnFrequency : float = 1.0;
var spawnCount : int = 3;

var firstPoint : Vector3;
var secondPoint : Vector3;

private var spawnTime : float = 0.0;
private var spawnToggle : float = -1.0;

var startPoint : Vector3;
var endPoint : Vector3;

function Update(){
	spawnTime -= Time.deltaTime;
	if(spawnTime <= 0.0) Spawn();
}

function Spawn(){
	spawnTime = spawnFrequency;
	var angle : float = Vector2.Angle(Vector2.up, endPoint - startPoint);
	var distance : float = (endPoint - startPoint).magnitude;
	if(endPoint.x > startPoint.x) angle = -angle;
	
	var cnt : int = 0;
	var offsetX : float = spawnCount / 2 + 1.0;
	while(cnt < spawnCount){
		spawnToggle = -spawnToggle;
		var newLine : Line = Instantiate(linePrefab, startPoint, Quaternion.Euler(0, 0, angle)).transform.Find("Line").GetComponent(Line);
		newLine.transform.parent.rotation = Quaternion.Euler(0, 0, angle + (-spawnToggle * 10));
	
		newLine.xAmp = spawnToggle * 4;
		newLine.yAmp = distance / 13 / offsetX;
		newLine.xOffset = offsetX;
		newLine.spiralOffset = 0.0;
		newLine.yTurn = 1.0;
		newLine.tExp = 1.0;
		
		if(endPoint == startPoint){
		/*	newLine.xAmp = 4.0;
			newLine.yAmp = 4.0;
			newLine.xOffset = 0.0;
			newLine.yTurn = 0.0;
			newLine.tExp = 0.0;
			newLine.spiralOffset = Mathf.PingPong(cnt * Mathf.PI, Mathf.PI); 
			newLine.transform.parent.rotation = Quaternion.identity;*/
		}
		if(spawnToggle < 0) offsetX -= 1.0;
		cnt++;
	}
}