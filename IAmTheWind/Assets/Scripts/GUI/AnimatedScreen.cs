﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IATW.GUI {

public class AnimatedScreen : MonoBehaviour {
	public event System.Action OnAnimationComplete;

	public void Run() {
		gameObject.SetActive(true);
		StartCoroutine(WaitThenFire());
	}

	IEnumerator WaitThenFire() {
		yield return new WaitForSeconds(3.0f);
		if(OnAnimationComplete != null) OnAnimationComplete();
	}
}

}