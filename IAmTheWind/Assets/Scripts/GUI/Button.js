var normalImage : Texture2D;
var hoverImage : Texture2D;
var activeImage : Texture2D;

var target : GameObject;
var selector : String;

var sendOnUp : boolean = true;

function Awake(){
	GetComponent.<GUITexture>().texture = normalImage;
}

function OnMouseDown(){
	GetComponent.<GUITexture>().texture = activeImage;
	if(!sendOnUp) target.SendMessage(selector, SendMessageOptions.DontRequireReceiver);
}

function OnMouseOver(){
	if(!Input.GetMouseButton(0)) GetComponent.<GUITexture>().texture = hoverImage;
}

function OnMouseExit(){
	GetComponent.<GUITexture>().texture = normalImage;
}

function OnMouseUpAsButton(){
	GetComponent.<GUITexture>().texture = normalImage;
	if(sendOnUp) target.SendMessage(selector, SendMessageOptions.DontRequireReceiver);
}