var isToggle : boolean = true;
var powerObjects : GameObject[];
var onRenderer : Renderer;
var offRenderer : Renderer;

private var isOn : boolean;

function Update(){
	for(var curPO : GameObject in powerObjects){
		if(isOn) curPO.SendMessage("Power");
	}
	onRenderer.enabled = isOn;
	offRenderer.enabled = !isOn;
}

function OnTriggerEnter(col : Collider){
	SwitchEnter();
}

function OnTriggerExit(col : Collider){
	SwitchExit();
}

function OnCollisionEnter(col : Collision){
	SwitchEnter();
}

function OnCollisionExit(col : Collision){
	SwitchExit();
}

function SwitchEnter(){
	if(!isToggle) isOn = true;
	else isOn = !isOn;
}

function SwitchExit(){
	if(!isToggle) isOn = false;
}