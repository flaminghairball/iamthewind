var nodeGroups : NodeGroup[];

@script ExecuteInEditMode()


enum CapType {
	None,
	Left,
	Right
}

class Node extends Object{
	var pos : Vector3 = Vector3.zero;
	var breakPath : boolean = false; //should we link this node to the others
	var baselineDistance : float = 0.0;
	
	function Node(createPos : Vector3){
		pos = createPos;
	}
}

class NodeGroup extends Object{
	var displayName : String = "New node group";
	var displayImage : Texture2D;
	var parentTransform : Transform;
	var capUVRect : Vector4;
	var uvRect : Vector4;
	var material : Material;
	var tileSize : Vector2;
	var baselineTileSize : Vector2;
	var randomRotFactor : float;
	var lockColliderZ : float;
	var colliderInset : Vector4; //inset values 
	var extrudeZAxis : boolean;
	
	var createBaseline : boolean;
	var generateBaselineColliders : boolean;
	var baselineDistance : float;
	var capOutset : float;
	var capWidth : float;
	var maxBaselineYGap : float;
	var capUV : boolean;
	var isMesh : boolean;
	var layer : int;
	
	var nodes : Node[] = new Node[0];

	var createNodePos : Vector3 = Vector3.zero; //the position at which to create a new node
	
	var displayEditor : boolean;
	
	private var displayObject : GameObject;
	private var baselineObject : GameObject;
		
	//state vars for pushvertstomesh()
	private var opVerts : Array;
	private var opUV : Array;
	private var opTris : Array;
	private var polyZToggle : float;
		
	function NodeGroup(){
		displayName = "NodeGroup";
		tileSize = Vector2.one * 2;
		baselineTileSize = Vector2.one * 2;
		createBaseline = true;
		baselineDistance = 10.0;
		maxBaselineYGap = 4.0;
		displayEditor = false;
		capOutset = 0.5;
		capWidth = 1.0;
		capUV = true;
		uvRect = Vector4(0, 0, 1, 1);
		capUVRect = Vector4(0, 0, 1, 1);
		shouldCap = false;
		polyZToggle = 0.1;
		lockColliderZ = 0.0;
		generateBaselineColliders = false;
		extrudeZAxis = false;
		colliderInset = Vector4.zero;
		isMesh = true;
		layer = 0;
	}
	
	function AddNode(){
		var newNodes : Array = new Array(nodes);
		newNodes.Push(new Node(createNodePos));
		nodes = newNodes.ToBuiltin(Node);
		
		createNodePos -= Vector3.right * 4;
	
		if(nodes.length > 1) UpdateMesh();
	}
	
	function UnshiftNode(){
		var newNodes : Array = new Array(nodes);
		newNodes.Unshift(new Node(nodes[0].pos + Vector3.right * 2));
		nodes = newNodes.ToBuiltin(Node);
		if(nodes.length > 1) UpdateMesh();
	}
	
	function KillNodeAtIndex(index : int){
		var opNodes : Array = new Array(nodes);
		opNodes.RemoveAt(index);
		nodes = opNodes.ToBuiltin(Node);
		UpdateMesh();
	}
	
	function InsertNodeAtIndex(index : int){
		var opNodes : Array = new Array();
		var nodeIndex : int = 0;
		for(var curNode in nodes){
			if(nodeIndex == index){
				opNodes.Push(new Node(curNode.pos + Vector3.right * 4));
			}
			opNodes.Push(curNode);

			nodeIndex++;
		}
		nodes = opNodes.ToBuiltin(Node);
		UpdateMesh();
	}
	
	function BreakPathAtIndex(nodeIndex : int){
		if(nodeIndex > (nodes.length - 1)) {
			Debug.Log("Node index is out of range.");
			return;
		}
		nodes[nodeIndex].breakPath = true;
	}
	
	function UpdateMesh(){
		if(!isMesh) return; //we don't create meshes if we're solely a collision node group
		UpdateLastNodePos();
		if(!displayObject) CreateGameObject();
		if(material) displayObject.GetComponent.<Renderer>().material = material;
		
		opVerts = new Array();
		opTris = new Array();
		opUV = new Array();
		
		var i : int;
		var wantedBaseY : float = 0.0;
		maxeBaselineYGap = tileSize.y;
		for(i = 0; i < nodes.length - 1; i++){
			var curNode : Node = nodes[i];
			if(curNode.breakPath) {
				wantedBaseY = 0.0;
				continue;
			}
			wantedBaseY += curNode.baselineDistance;
			var previousNode : Node = nodes[Mathf.Clamp(i - 1, 0, nodes.length - 1)];
			var nextNode : Node = nodes[Mathf.Clamp(i + 1, 0, nodes.length - 1)];
			
			var pos : Vector3 = curNode.pos;
			var curVec2 : Vector2 = Vector2(curNode.pos.x, curNode.pos.y);
			var nextVec2 : Vector2 = Vector2(nextNode.pos.x, nextNode.pos.y);;
			var angle : float = -Mathf.Asin((nextNode.pos.y - curNode.pos.y) / ((nextNode.pos - curNode.pos).magnitude + 0.001)) * Mathf.Rad2Deg;
			var rot : Quaternion = Quaternion.Euler(0, 0, angle);
			var capType : CapType = CapType.None;
			if(i == 0) capType = CapType.Left;
			else if(previousNode.breakPath) capType = capType.Left;
			
			var lerpPos : float = 0.0;
			var lerpUnit : float = Mathf.Clamp(tileSize.x / (nextNode.pos - curNode.pos).magnitude, 0.01, 1.0);
			while(lerpPos < 1.0){
				pos = Vector3.Lerp(curNode.pos, nextNode.pos, lerpPos);
				lerpPos += lerpUnit;
				if((nextNode.breakPath || i == nodes.length - 2) && lerpPos > 1.0) capType = CapType.Right;
				AddPoly(pos, rot, capType, false);	//generate edge polys
				if(createBaseline){
					var baselineY : float = 0.0;
					while(baselineY < wantedBaseY){
						baselineY += baselineTileSize.y;
						var baselinePolyPos : Vector3 = pos - Vector3.up * baselineY; // + -Vector3.forward * baselineY / 10;
						AddPoly(baselinePolyPos, Quaternion.identity, capType, true); //generate baseline polys
					}
				}
				capType = CapType.None;
			}
		}
		var newMesh : Mesh = new Mesh();
		newMesh.vertices = opVerts;
		newMesh.uv = opUV;
		newMesh.triangles = opTris;
		displayObject.GetComponent(MeshFilter).sharedMesh = newMesh;
		displayObject.GetComponent(MeshFilter).sharedMesh.RecalculateNormals();
		displayObject.GetComponent(MeshFilter).sharedMesh.RecalculateBounds();
	}
	
	function AddPoly(pos : Vector3, rot : Quaternion, capType : CapType, isBaseline : boolean){		
		var vertIndex : int = opVerts.length;
		
		//verts for top line
		var wantedTileSize : Vector2 = isBaseline ? baselineTileSize : tileSize;
		opVerts.Push(pos + rot * Vector3(-wantedTileSize.x , -wantedTileSize.y / 2, polyZToggle));
		opVerts.Push(pos + rot * Vector3(-wantedTileSize.x, wantedTileSize.y / 2, polyZToggle));
		opVerts.Push(pos + rot * Vector3(0, wantedTileSize.y / 2, polyZToggle));
		opVerts.Push(pos + rot * Vector3(0, -wantedTileSize.y / 2, polyZToggle));
		
		
		var uvMinX : float = (capType != CapType.None) ? (capType == CapType.Left ? capUVRect.x : capUVRect.z) : uvRect.x;
		var uvMaxX : float = (capType != CapType.None) ? (capType == CapType.Left ? capUVRect.z : uvRect.x) : uvRect.z;
		
		var uvMinY : float = isBaseline ? capUVRect.y : uvRect.y;
		var uvMaxY : float = isBaseline ? capUVRect.w : uvRect.w;
		
		//uv for top line
		opUV.Push(Vector2(uvMinX, uvMinY));
		opUV.Push(Vector2(uvMinX, uvMaxY));
		opUV.Push(Vector2(uvMaxX, uvMaxY));
		opUV.Push(Vector2(uvMaxX, uvMinY));
		
		//tris for top line
		opTris.Push(vertIndex + 2);
		opTris.Push(vertIndex + 1);
		opTris.Push(vertIndex + 0);
		opTris.Push(vertIndex + 0);
		opTris.Push(vertIndex + 3);
		opTris.Push(vertIndex + 2);
		
		//if(isBaseline) polyZToggle = -polyZToggle;
	}
	
	function UpdateLastNodePos(){
		nodes[nodes.length -1].pos = createNodePos;
	}
	
	function CreateGameObject(){
		if(!displayObject){
			if(parentTransform){
				if(parentTransform.Find(displayName)) displayObject = parentTransform.Find(displayName).gameObject;
			}
		}
		if(!displayObject) displayObject = new GameObject();
		displayObject.name = displayName;
		displayObject.layer = layer;
		if(isMesh){
			if(!displayObject.GetComponent(MeshFilter)) displayObject.AddComponent(MeshFilter);
			if(!displayObject.GetComponent(MeshRenderer)) displayObject.AddComponent(MeshRenderer);
			displayObject.GetComponent.<Renderer>().material = material;
		} else {
			if(displayObject.GetComponent(MeshFilter)) UnityEngine.Object.DestroyImmediate(displayObject.GetComponent(MeshFilter));
			if(displayObject.GetComponent(MeshRenderer)) UnityEngine.Object.DestroyImmediate(displayObject.GetComponent(MeshRenderer));
		}
		if(parentTransform) displayObject.transform.parent = parentTransform;
	}
	
	function CreateColliders(){
		CreateGameObject(); //make sure we have a display object to parent to
		var colParent : Transform;
		if(displayObject.transform.Find("Colliders")) UnityEngine.Object.DestroyImmediate(displayObject.transform.Find("Colliders").gameObject);
		
		colParent = new GameObject().transform;
		colParent.parent = displayObject.transform;
		colParent.gameObject.name = "Colliders";
		colParent.transform.localPosition = Vector3.zero;
		
		var nodeIndex : int = -1;
		var nextNode : Node;
		for(var curNode in nodes){
			nodeIndex++;
			if(nodeIndex > nodes.length - 2) break;

			nextNode = nodes[nodeIndex + 1];
			var angle : float = Vector3.Angle(Vector3.up, nextNode.pos - curNode.pos);
			
			if(curNode.breakPath) continue;
			
			//handle collider gameobject pos and rot
			var newCol : GameObject = new GameObject();
			newCol.name = "" + nodeIndex;
			newCol.layer = layer;
			newCol.transform.parent = colParent;
			newCol.transform.position = curNode.pos;
			newCol.transform.position.z = lockColliderZ;	
			if(curNode.pos.x < nextNode.pos.x) angle = - angle;

			newCol.transform.rotation = Quaternion.Euler(0, 0, angle);
			
			//collider setup
			if(isMesh){
				var col : BoxCollider = newCol.AddComponent(BoxCollider);
				col.size.x = tileSize.y - colliderInset.y - colliderInset.w;
				if(generateBaselineColliders) col.size.y += curNode.baselineDistance;
				col.size.y = ((nextNode.pos - curNode.pos).magnitude) - colliderInset.x - colliderInset.z;
				
				col.center.y = (col.size.y / 2) + colliderInset.x;
				col.center.x = col.size.x / 2 - colliderInset.y;
			} else {
				var cCol : BoxCollider = newCol.AddComponent(BoxCollider);
				cCol.size.x = cCol.size.z = 1.0;
				cCol.size.y = (nextNode.pos - curNode.pos).magnitude;
				cCol.center.y = cCol.size.y / 2;
			}
			
		}
	}
	
	function ClearNodes(){
		nodes = new Node[0];
		AddNode();
		AddNode();
	}
}

function AddNodeGroup(){
	var newGroups : NodeGroup[] = new NodeGroup[((nodeGroups) ? nodeGroups.length + 1 : 1)];
	var newNodeGroup : NodeGroup = new NodeGroup();
	newNodeGroup.parentTransform = transform;
	
	if(nodeGroups){
		var i : int = 0;
		for(var curGroup in nodeGroups){
			newGroups[i] = curGroup;
			i++;
		}
	}
	newGroups[i] = newNodeGroup;
	nodeGroups = newGroups;
}
