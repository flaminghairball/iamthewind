var bloomTransform : Transform;
var divHeight : float = 1.0;
var stemWidth : float = 1.0;
var maxAmplitude : float = 1.0;
var minAmplitude = 0.5;
var amplitudeFallOff : float = 0.01;

var cycle : float = 1.0;

private var uvToggle : float = 1.0;
private var lastPos : Vector3 = Vector3.zero;
private var amplitude : float;

function Start(){
	amplitude = maxAmplitude;
	BeginMesh();
}

function Update(){
	if((bloomTransform.position.y - (lastPos.y + transform.position.y)) > divHeight){
		AddNode();
	}
}

function AddNode(){
	amplitude = Mathf.Clamp(amplitude - amplitudeFallOff, minAmplitude, maxAmplitude);
	//make some variables
	var oldMesh : Mesh = GetComponent(MeshFilter).mesh;
	var newVerts : Vector3[] = new Vector3[oldMesh.vertices.length + 2];
	var newUV : Vector2[] = new Vector2[oldMesh.uv.length + 2];
	var newTris : int[] = new int[oldMesh.triangles.length + 6];
	
	//copy old data
	var i : int = 0;
	for(var curVert in oldMesh.vertices){
		newVerts[i] = curVert;
		newUV[i] = oldMesh.uv[i];
		i++;
	}
	i = 0;
	for(var curTri in oldMesh.triangles){
		newTris[i] = curTri;
		i++;
	}
	
	//put in the new data
	var previousPos : Vector3 = Vector3.Lerp(newVerts[newVerts.length - 3], newVerts[newVerts.length - 4], 0.5);
	var y : float = newVerts.length * divHeight;
	lastPos = Vector3(amplitude * Mathf.Sin(cycle * y), y, 0.0);

	var rot : Quaternion = Quaternion.identity;
	newVerts[newVerts.length - 2] = lastPos + (rot * Vector3.right * stemWidth);
	newVerts[newVerts.length - 1] = lastPos - (rot * Vector3.right * stemWidth);
	newUV[newUV.length - 2] = Vector2(1.0, uvToggle);
	newUV[newUV.length - 1] = Vector2(0.0, uvToggle);
	newTris[newTris.length - 6] = newVerts.length - 2;
	newTris[newTris.length - 5] = newVerts.length - 3;
	newTris[newTris.length - 4] = newVerts.length - 4;
	newTris[newTris.length - 3] = newVerts.length - 3;
	newTris[newTris.length - 2] = newVerts.length - 2;
	newTris[newTris.length - 1] = newVerts.length - 1;
	
	uvToggle = Mathf.PingPong(uvToggle + 1.0, 1.0);
	
	var filter : MeshFilter = GetComponent(MeshFilter);
	var newMesh : Mesh = new Mesh();
	newMesh.vertices = newVerts;
	newMesh.uv = newUV;
	newMesh.triangles = newTris;
	newMesh.RecalculateNormals();
	filter.mesh = newMesh;
	filter.mesh.RecalculateBounds();
}

function BeginMesh(){
	var mesh : Mesh = new Mesh();
	mesh.vertices = [Vector3.right * stemWidth, -Vector3.right * stemWidth];
	mesh.uv = [Vector2(1.0, 0.0), Vector2(0.0, 0.0)];
	GetComponent(MeshFilter).mesh = mesh;
}

@script RequireComponent(MeshFilter, MeshRenderer);