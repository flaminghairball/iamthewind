var inactiveObject : GameObject; //the object that is activated when we're not
var activeObject : GameObject; //the object we activate when the collider triggers us
var activateTrigger : Collider; //the trigger that triggers us

private var trigCount : int = 0;

function OnTriggerEnter(col : Collider){
	trigCount++;
	if(activateTrigger){ if(col == activateTrigger) Activate(); }
	else if(trigCount > 0) Activate();
}

function OnTriggerExit(col : Collider){
	trigCount--;
	if(activateTrigger) { if(col == activateTrigger) Deactivate(); }
	else if(trigCount == 0) Deactivate();
}

function Activate(){
	activeObject.SetActiveRecursively(true);
	inactiveObject.SetActiveRecursively(false);
}

function Deactivate(){
	activeObject.SetActiveRecursively(false);
	inactiveObject.SetActiveRecursively(true);
}