private var winActivate : ActivateOnWin[];

var guyPrefab : GameObject;
var nextLevelDelay : float = 1.0;

private var spawnPoint : Transform;
private var guy : GameObject; //the guy who walks around
private var hasWon : boolean = false;

function Awake(){
	if(GameObject.FindGameObjectsWithTag("GameController").length > 1 && gameObject.name != "_GameController") Destroy(gameObject);
}

function LoadFirstLevel(){
	Application.LoadLevel(1);
}

function OnLevelWasLoaded(level : int){
	if(GameObject.FindWithTag("Respawn")) spawnPoint = GameObject.FindWithTag("Respawn").transform;
	
	winActivate = FindObjectsOfType(ActivateOnWin) as ActivateOnWin[];
	for(var curAct in winActivate){
		curAct.gameObject.SetActiveRecursively(!curAct.activate);
	}
	
	//DEBUG ONLY
	/*var controllers = GameObject.FindGameObjectsWithTag("GameController");
	for(var curController in controllers){ //nuke all duplicate controllers if they're not legit
		if(curController != gameObject && curController.gameObject.name != "_GameController") Destroy(curController);
	}*/
}

function GuyArrived(){
	hasWon = true;
	if(winActivate){
		for(var curAct in winActivate){
			curAct.gameObject.SetActiveRecursively(curAct.activate);
		}
	}
	yield WaitForSeconds(nextLevelDelay);
	hasWon = false;
	Application.LoadLevel(Mathf.Repeat(Application.loadedLevel + 1, Application.levelCount));
}

function Update(){
	if(Input.GetKeyDown("r")) Application.LoadLevel(Application.loadedLevel);
	if(!guy &! hasWon){ //make sure we have a guy if we need one
		if(!GameObject.FindWithTag("Player")){
			if(!spawnPoint){
				if(Application.loadedLevel > 0) {
					Debug.Log("Killing Controller because I can't create player)");
					Destroy(gameObject);
				}
			} else {
				guy = Instantiate(guyPrefab, spawnPoint.position, spawnPoint.rotation);
			}
		} else {
			guy = GameObject.FindWithTag("Player");
		}
	}
}