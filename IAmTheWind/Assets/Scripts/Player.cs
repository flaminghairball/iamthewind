﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IATW {

public class Player : MonoBehaviour {
	public event System.Action OnDeath;
	public event System.Action OnWin;
	
	void OnCollisionEnter(Collision collision) {
		if(collision.gameObject.CompareTag(Constants.Tags.KillCollider)) {
			OnDeath?.Invoke();
		} 
	}

	void OnTriggerEnter(Collider collider) {
		if(collider.gameObject.CompareTag(Constants.Tags.HouseCollider)) {
			OnWin?.Invoke();
		}
	}
}

}