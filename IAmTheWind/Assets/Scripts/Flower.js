var growSpeed : float = 0.01;

function OnParticleCollision(other : GameObject){
	if(other.CompareTag("Rain")){
		GetComponent.<Rigidbody>().MovePosition(GetComponent.<Rigidbody>().position + Vector3.up * growSpeed);
	}
}