﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace IATW.Gameplay {

public interface ILayCollidersAlongPath { 
	void SetPath(List<Vector3> path);
	CapsuleCollider[] colliders { get; }
}

public class LayCollidersAlongPath : ILayCollidersAlongPath {
	public CapsuleCollider[] colliders { get; private set; }
	Settings m_settings;

	public LayCollidersAlongPath(Settings settings) {
		m_settings = settings;
		colliders = Enumerable.Range(0, settings.colliderCount)
								.Select(x => UnityEngine.Object.Instantiate(settings.colliderPrefab))
								.ToArray();
	}

	public void SetPath(List<Vector3> path) {
		for(int i = 0; i < colliders.Length; i++) {
			colliders[i].enabled = i < (path.Count - 1);
		}
		if(path.Count < 2) return;

		for(int i = 0; i < colliders.Length && i < path.Count - 1; i++) {
			colliders[i].transform.position = path[i] + (path[i + 1] - path[i]) * 0.5f;
			colliders[i].transform.up = path[i + 1] - path[i];
			colliders[i].height = (path[i + 1] - path[i]).magnitude;
		}
	}

	[Serializable]
	public class Settings {
		public int colliderCount;
		public CapsuleCollider colliderPrefab;
	}
}

}