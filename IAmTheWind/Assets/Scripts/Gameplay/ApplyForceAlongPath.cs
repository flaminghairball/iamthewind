﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace IATW.Gameplay {

public interface IApplyForceAlongPath {
	void SetPath(List<Vector3> path);
}

public class ApplyForceAlongPath : IApplyForceAlongPath, Zenject.IFixedTickable {
	public Collider[] colliders { get; private set; }
	Settings m_settings;
	
	List<Vector3> m_path;

	public ApplyForceAlongPath(Settings settings) {
		m_settings = settings;
		colliders = new Collider[m_settings.maxPathCount];
	}

	public void SetPath(List<Vector3> path) {
		m_path = path;
	}

	public void FixedTick() {
		if(m_path == null) return;
		if(m_path.Count < 2) return;

		for(int i = 0; i < m_path.Count - 1; i++) {
			var vec = (m_path[i + 1] - m_path[i]).normalized;
			var colliderCount = Physics.OverlapCapsuleNonAlloc(m_path[i], m_path[i+1], m_settings.overlapRadius, colliders, m_settings.layerMask, QueryTriggerInteraction.Collide);

			for(int x = 0; x < colliderCount; x++) {
				var body = colliders[x].attachedRigidbody;
				if(body != null && !body.isKinematic) {
					body.AddForce(vec * m_settings.force, m_settings.forceMode);
				}
			}
		}
	}

	[Serializable]
	public class Settings {
		public int maxPathCount = 10;
		public float force = 1.0f;
		public float overlapRadius = 1.0f;
		public ForceMode forceMode = ForceMode.Force;
		public LayerMask layerMask;
	}
}

}