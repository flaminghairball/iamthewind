﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace IATW.Gameplay {

public interface IWind { }

public class Wind : IWind {
    Camera m_camera;
	IPathGenerator m_pathGenerator;
    ISampler m_pathSampler;
    LineRenderer m_lineRenderer;

    Plane m_gamePlane;

    public Wind(Camera camera,
                IPathGenerator pathGenerator, 
                ApplyForceAlongPath applyForceAlongPath,
                ISampler sampler, 
                [Inject(Id=Constants.WindLinePrefabId)] LineRenderer lineRenderer) {
        m_gamePlane = new Plane(Vector3.forward, Vector3.zero);
        m_camera = camera;
        m_pathGenerator = pathGenerator;
        m_pathSampler = sampler;
        m_lineRenderer = lineRenderer;
        
        m_lineRenderer.positionCount = 0;

        m_pathSampler.onSample += (point, timestamp) => {
            if(!m_pathSampler.isDown) return;

            var camRay = m_camera.ScreenPointToRay(point);
            var hitDistance = 0.0f;
            if(m_gamePlane.Raycast(camRay, out hitDistance)) {
                m_pathGenerator.AddSample(camRay.GetPoint(hitDistance), Time.time);
            }
        };

        m_pathSampler.onUp += m_pathGenerator.Clear;

        m_pathGenerator.onPathUpdated += (path) => {
            m_lineRenderer.positionCount = path.Count;
            m_lineRenderer.SetPositions(path.ToArray());
            applyForceAlongPath.SetPath(path);  
        };
    }
}

}