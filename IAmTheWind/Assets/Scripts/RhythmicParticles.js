var time : float = 1.0;
var animate : Animation;
var clip : AnimationClip;

private var emitTime : float;

function Update () {
	emitTime -= Time.deltaTime;
	if(emitTime <= 0.0){
		if(clip && animate) emitTime = animate[clip.name].length / time;
		else emitTime = time;
		GetComponent.<ParticleEmitter>().Emit();
	}
}