//Constants
//As we go, these can probable be split out into more specific classes

namespace IATW {

public class Constants {
    //zenjection constants
    public const string WindLinePrefabId = "WindLinePrefab";

    //gui
    public const string LoseScreenId = "LoseScreenId";
    public const string WinScreenId = "WinScreenId";


    public class Tags {
        public const string KillCollider = "KillCollider";
        public const string HouseCollider = "Finish";
    }
}

}