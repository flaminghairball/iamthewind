﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;
using IATW.GUI;

namespace IATW {

public class LevelController {
	Player player; 

	public LevelController(Player player, 
							LevelSettings levelSettings,
							[Inject(Id = Constants.LoseScreenId)] AnimatedScreen loseScreen,
							[Inject(Id = Constants.WinScreenId)] AnimatedScreen winScreen) {
		this.player = player;
		player.OnDeath += loseScreen.Run;
		player.OnWin += winScreen.Run;

		loseScreen.OnAnimationComplete += () => SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		winScreen.OnAnimationComplete += () => SceneManager.LoadScene(levelSettings.nextSceneName);
	}
}

}