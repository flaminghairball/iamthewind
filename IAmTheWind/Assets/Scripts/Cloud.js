var rainEmitter : ParticleEmitter;
var normalBrightness : Color = Color.white;
var rainingBrightness : Color = Color.gray;
var rainStartSpeed : float = 1.0;
var bounceOffForce : float = 1.0; 
var idleVelocity : Vector3 = Vector3.right * 0.01;

private var particleForce : Vector3 = Vector3.zero;
private var rainTime : float = 0.0;
private var collideNormal : Vector3 = Vector3.zero;

function Start(){
	if(!rainEmitter) rainEmitter = GetComponentInChildren(ParticleEmitter);
}

function FixedUpdate () {
	rainTime = Mathf.Clamp(rainTime + Time.deltaTime * ((collideNormal != Vector3.zero) ? rainStartSpeed : -rainStartSpeed), 0, 1.0); //add or kill raintime based on whether or not we're colliding
	rainEmitter.emit = (rainTime == 1.0);
	rainEmitter.worldVelocity = particleForce;
	GetComponent.<Renderer>().material.color = Color.Lerp(normalBrightness, rainingBrightness, rainTime);
	if(GetComponent.<Rigidbody>().velocity == Vector3.zero) GetComponent.<Rigidbody>().MovePosition(GetComponent.<Rigidbody>().position + idleVelocity);
	if(collideNormal != Vector3.zero) GetComponent.<Rigidbody>().AddForce(collideNormal * bounceOffForce);
}

function OnCollisionEnter(col : Collision){
	collideNormal = col.contacts[0].normal;
}

function OnCollisionExit(col : Collision){
	collideNormal = Vector3.zero;
}

function Wind(force : Vector3){
	particleForce = force * 10;
}