 Shader "Terrain" {
         Properties {
                 _Color ("Main Color", Color) = (.5, .5, .5, .5)
                 _MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
                 _Cutoff ("Base Alpha cutoff", Range (0,.9)) = .5
         }
         SubShader {
         		Tags{"LightMode" = "Vertex"}
                 // Set up basic lighting
                 Material {
                 		 Emission[_Color]
                         Ambient [_Color]
                 }       
                 Lighting On
 
                 // Render both front and back facing polygons.
                 Cull Back
 
                 // first pass:
                 //   render any pixels that are more than [_Cutoff] opaque
                 Pass {  
                         AlphaTest Greater [_Cutoff]
                         SetTexture [_MainTex] {
                                 combine texture * primary, texture
                         }
                 }
 
                 // Second pass:
                 //   render in the semitransparent details.
                 Pass {
                         // Dont write to the depth buffer
                         ZWrite off
                         
                         // Only render pixels less or equal to the value
                         AlphaTest LEqual [_Cutoff]
                         
                         // Set up alpha blending
                         Blend SrcAlpha OneMinusSrcAlpha
 
                         SetTexture [_MainTex] {
                                 combine texture * primary, texture
                         }
                 }
         }
 }
